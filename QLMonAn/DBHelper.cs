﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLMonAn
{
    public class DBHelper
    {
        public QuanLyMonAnEntities db;

        public DBHelper()
        {
            db = new QuanLyMonAnEntities();
        }

        public List<HinhAnh> GetHinhAnhs()
        {
            List<HinhAnh> hinhanh = ((from ha in db.HinhAnhs
                                      select ha)).ToList();
            return hinhanh;
        }

        public List<HinhAnh> GetHinhAnhsById(int id)
        {
           
            return (from ha in db.HinhAnhs
                                     where ha.MaMonAn == id
                                     select ha).ToList();
        }

        public void AddHinhAnh(HinhAnh ha)
        {
            db.HinhAnhs.Add(ha);
            db.SaveChanges();
        }
        public IEnumerable<MonAn> GetMonAn()
        {
            var ha = from ma in db.MonAns
                   select ma;
            return ha;
        }
        public MonAn GetMonAnById(int id)
        {
            return db.MonAns.Find(id);
        }
        public IEnumerable<MonAn> GetMonAnsDesByName()
        {
            var monan = (from ma in db.MonAns
                         select ma).OrderByDescending(m => m.TenMonAn);
            return monan;
        }

        public IEnumerable<MonAn> GetMonAnsDesByNameAndId(int id)
        {
            return (from ma in db.MonAns
                    select ma).Where(ma => ma.LoaiMonAn == id).OrderByDescending(m => m.TenMonAn);
        }

        public IEnumerable<MonAn> GetMonAnsDesByNameAndXuatxu(string xuatxu)
        {
            return (from ma in db.MonAns
                    select ma).Where(ma => ma.XuatXu == xuatxu).OrderByDescending(m => m.TenMonAn);
        }

        public void AddMonAn(MonAn ma)
        {
            db.MonAns.Add(ma);
            db.SaveChanges();
        }

        public void RemoveMonAn(MonAn ma)
        {
            db.MonAns.Remove(ma);
            db.SaveChanges();
        }
        public List<LoaiMonAn> GetLoaiMonAn()
        {
            return db.LoaiMonAns.ToList();
        }

        public List<LoaiMonAn> GetLoaiMonAnById(int id)
        {
            return (from lma in db.LoaiMonAns
                    where lma.MaLoaiMonAn == id
                    select lma).ToList();
        }

        public IEnumerable<MonAn> GetMonAnsContainName(string pTenMonAn)
        {
            return (from ma in db.MonAns
                    select ma).Where(ma => ma.TenMonAn.Contains(pTenMonAn)).OrderByDescending(m => m.TenMonAn);
        }
        public List<Comment> GetCommentById(int id)
        {
            return (from bl in db.Comments
                    where bl.MaMonAn == id
                    select bl).ToList();
        }
        public void AddComment(Comment cm)
        {
            db.Comments.Add(cm);
            db.SaveChanges();
        }
        public List<GioHangTamThoi> GetGioHang()
        {
            return (from gh in db.GioHangTamThois
                    select gh).ToList();
        }
        public GioHangTamThoi GetGioHangById(int id)
        {
            return db.GioHangTamThois.Find(id);
        }
        public void AddGioHang(GioHangTamThoi gh)
        {
            db.GioHangTamThois.Add(gh);
            db.SaveChanges();
        }
        public void RemoveGioHang(GioHangTamThoi gh)
        {
            db.GioHangTamThois.Remove(gh);
            db.SaveChanges();
        }
        public DonDatHang GetDonDatHangById(int id)
        {
            return db.DonDatHangs.Find(id);
        }

        public void RemoveDDH(DonDatHang ddh)
        {
            db.DonDatHangs.Remove(ddh);
            db.SaveChanges();
        }

        public AspNetUser GetUserByUserName(string UserName)
        {
            return (from u in db.AspNetUsers
                    where u.UserName == UserName
                    select u).FirstOrDefault(); 
        }

        public void AddDDH(DonDatHang ddh)
        {
            db.DonDatHangs.Add(ddh);
            db.SaveChanges();
        }

        public List<AspNetUser> GetUser()
        {
            return (from user in db.AspNetUsers
                    select user).ToList();
        }

        public List<MonAn> Getmonan()
        {
            return (from ma in db.MonAns
                            select ma).ToList();
        }

        public List<DonDatHang> GetDDH()
        {
            return (from ddh in db.DonDatHangs
                    select ddh).ToList();
        }

        public IEnumerable<MonAn> GetMonAnTop10()
        {
            return (from ma in db.MonAns
                    select ma).OrderByDescending(m => m.SoLuongBan).ToList().Take(10);
        }
    }
}
