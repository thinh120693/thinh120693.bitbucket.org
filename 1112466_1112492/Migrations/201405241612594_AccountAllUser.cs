namespace _1112466_1112492.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountAllUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "GioiTinh", c => c.String());
            AddColumn("dbo.AspNetUsers", "NoiO", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "NoiO");
            DropColumn("dbo.AspNetUsers", "GioiTinh");
        }
    }
}
