﻿using System.ComponentModel.DataAnnotations;

namespace _1112466_1112492.Models
{
    public class DangKyModels
    {
        [Required]
        [Display(Name = "Tên tài khoản")]
        [StringLength(100, ErrorMessage = "Tên tài khoản có độ dài từ 6 -> 20 kí tự", MinimumLength = 6)]
        public string TenTaiKhoan { get; set; }


        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu")]
        [StringLength(100, ErrorMessage = "Mật khẩu có độ dài từ 6 -> 20 kí tự", MinimumLength = 6)]
        public string MatKhau { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Nhập lại mật khẩu")]
        [StringLength(100, ErrorMessage = "Mật khẩu có độ dài từ 6 -> 20 kí tự", MinimumLength = 6)]
        [Compare("MatKhau")]
        public string NhapLaiMatKhau { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(11, ErrorMessage = "Điện thoại liên lạc chỉ gồm số và có từ 6 -> 11 số", MinimumLength = 6)]
        [Phone]
        [Display(Name = "Điện thoại")]

        public string DienThoai { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "Chưa chọn ngày tháng năm sinh")]
        [Display(Name = "Ngày sinh")]
        public string NgaySinh { get; set; }
    }
}