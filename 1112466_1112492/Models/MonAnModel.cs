﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace _1112466_1112492.Models
{
    public class MonAnModel
    {
        [Required]
        [Display(Name = "Tên món ăn")]
        [StringLength(50, ErrorMessage = "Tên món ăn có độ dài từ 6 -> 50 kí tự", MinimumLength = 6)]
        public string TenMonAn { get; set; }


        [Required]
        [RegularExpression(@"[0-9]*\.?[0-9]+", ErrorMessage = "{0} must be a Number.")]
        [Display(Name = "Đơn giá")]

        public string DonGia { get; set; }


        [Required]
        [Display(Name = "Mô tả")]
        [StringLength(1000, ErrorMessage = "Mô tả món ăn có độ dài từ 1 -> 1000 kí tự", MinimumLength = 1)]
        public string MoTa { get; set; }

        [Required]
        [Display(Name = "Link hình ảnh 1")]
        [StringLength(100, ErrorMessage = "Link hình ảnh có độ dài từ 1 -> 100 kí tự", MinimumLength = 1)]
        public string linkhinhanh1 { get; set; }
        public string linkhinhanh2 { get; set; }
        public string linkhinhanh3 { get; set; }

        public int MaMonAn { get; set; }
        public bool TinhTrang { get; set; }

        public string XuatXu { get; set; }

        public int LoaiThucAn { get; set; }

        public QLMonAn.HinhAnh ha1 { get; set; }

        public QLMonAn.HinhAnh ha2 { get; set; }

        public QLMonAn.HinhAnh ha3 { get; set; }

    }
}