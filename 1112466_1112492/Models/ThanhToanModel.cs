﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _1112466_1112492.Models
{
    public class ThanhToanModel
    {
        [Required]
        [Display(Name = "Tên tài khoản")]
        public string TenTaiKhoan { get; set; }

        [Required]
        [Display(Name = "Địa chỉ")]
        public string DiaChi { get; set; }

        [Required]
        [Display(Name = "Số điện thoại")]
        public string SoDienThoai { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Số tiền thanh toán")]
        public int SoTien { get; set; }

        [Required]
        [Display(Name = "Ngày giao hàng")]
        public string NgayGiaoHang { get; set; }
    }
}