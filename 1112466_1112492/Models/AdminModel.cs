﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _1112466_1112492.Models
{
    public class AdminModel
    {
        public List<QLMonAn.AspNetUser> users;
        public List<QLMonAn.MonAn> monans;
        public List<QLMonAn.DonDatHang> ddhs;

        public AdminModel()
        {
            users = new List<QLMonAn.AspNetUser>();
            monans = new List<QLMonAn.MonAn>();
            ddhs = new List<QLMonAn.DonDatHang>();
        }
    }
}