﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _1112466_1112492.Models
{
    public class ThongKeModel
    {
        public List<QLMonAn.DonDatHang> ddhs;
        public IEnumerable<QLMonAn.MonAn> monans;
        public int DoanhSo;


        public ThongKeModel()
        {
            ddhs = new List<QLMonAn.DonDatHang>();
            monans = new List<QLMonAn.MonAn>();
            DoanhSo = 0;
        }
    }
}