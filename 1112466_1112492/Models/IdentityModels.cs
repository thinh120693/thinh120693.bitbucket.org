﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace _1112466_1112492.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string Email { get; set; }

        public string DienThoai { get; set; }

        public string NgaySinh { get; set; }

        public string NoiO { get; set; }

        public string GioiTinh { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }
    }
}