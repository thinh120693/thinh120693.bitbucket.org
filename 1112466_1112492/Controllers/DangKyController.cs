﻿using _1112466_1112492.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _1112466_1112492.Controllers
{
    public class DangKyController : Controller
    {
        QLMonAn.QuanLyMonAnEntities db = new QLMonAn.QuanLyMonAnEntities();
        //
        // GET: /DangKy/
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult KetQuaDangKy(DangKyModels model)
        {
            //ViewBag.Message = "Dang ky thanh cong";
            ViewBag.TenDangNhap = model.TenTaiKhoan;

            ViewBag.MatKhau = model.MatKhau;

            ViewBag.NhapLaiMatKhau = model.NhapLaiMatKhau;
            
            ViewBag.Email = model.Email;

            if (Request["cbGioiTinh"] == "nam")
                ViewBag.GioiTinh = "Nam";
            else if (Request["cbGioiTinh"] == "nu")
                ViewBag.GioiTinh = "Nữ";

            ViewBag.NgaySinh = Request["datepicker"];

            if (Request["cbNoiO"] == "hcm")
                ViewBag.NoiO = "Hồ Chí Minh";
            else if (Request["cbNoiO"] == "hn")
                ViewBag.NoiO = "Hà Nội";
            else if (Request["cbNoiO"] == "hue")
                ViewBag.NoiO = "Huế";

            ViewBag.DienThoai = model.DienThoai;
            return View();
        }

        [HttpPost]
        public ActionResult KetQuaTaoMonAn(MonAnModel model)
        {
            QLMonAn.MonAn ma = new QLMonAn.MonAn();
            try
            {
                ma.TenMonAn = model.TenMonAn;
                ma.DonGia = Int32.Parse(model.DonGia);
                ma.MoTa = model.MoTa;
                if (Request["cbLoaiMonAn"] == "heo")
                {
                    ma.LoaiMonAn = 1;
                }
                else if (Request["cbLoaiMonAn"] == "bo")
                    ma.LoaiMonAn = 2;
                else if (Request["cbLoaiMonAn"] == "ga")
                    ma.LoaiMonAn = 3;
                else if (Request["cbLoaiMonAn"] == "haisan")
                    ma.LoaiMonAn = 4;

                if (Request["tinhtrang"] == "ss")
                    ma.TinhTrang = true;
                else
                    ma.TinhTrang = false;

                if (Request["odau"] == "bac")
                    ma.XuatXu = "Miền Bắc";
                else if (Request["odau"] == "trung")
                    ma.XuatXu = "Miền Trung";
                else if (Request["odau"] == "nam")
                    ma.XuatXu = "Miền Nam";
                else if (Request["odau"] == "tay")
                    ma.XuatXu = "Miền Tây";

                db.MonAns.Add(ma);
                db.SaveChanges();

                int id = ma.MaMonAn;

                QLMonAn.HinhAnh ha = new QLMonAn.HinhAnh();
                ha.MaMonAn = id;
                ha.LinkHinhAnh = model.linkhinhanh1;
                db.HinhAnhs.Add(ha);
                db.SaveChanges();

                if (model.linkhinhanh2 != null)
                {
                    ha.MaMonAn = id;
                    ha.LinkHinhAnh = model.linkhinhanh2;
                    db.HinhAnhs.Add(ha);
                    db.SaveChanges();
                }

                if (model.linkhinhanh3 != null)
                {
                    ha.MaMonAn = id;
                    ha.LinkHinhAnh = model.linkhinhanh3;
                    db.HinhAnhs.Add(ha);
                    db.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {
                
            }
            

            
            return RedirectToAction("Index", "MonAn");
        }
	}
}