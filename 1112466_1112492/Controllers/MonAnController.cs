﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QLMonAn;
using System.Net;
using PagedList;
using System.Data.Entity;
using System.Data.Entity.Validation;
using Microsoft.AspNet.Identity;
namespace _1112466_1112492.Controllers
{
    public class MonAnController : Controller
    {
        private QuanLyMonAnEntities db = new QuanLyMonAnEntities();
        private DBHelper dbhelper = new DBHelper();
        private List<MonAn> kqMonAn = new List<MonAn>();

        //
        // GET: /MonAn/
        public ActionResult Index(int? page, string UserName)
        {
            ViewBag.UserName = UserName;
            
            int pageSize = 3;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            List<HinhAnh> hinhanh = dbhelper.GetHinhAnhs(); //((from ha in db.HinhAnhs
                                     // select ha)).ToList();

            IPagedList<MonAn> monan = //(from ma in db.MonAns
                                       //select ma).OrderByDescending(m => m.TenMonAn)
                                    dbhelper.GetMonAnsDesByName().ToPagedList(pageIndex, pageSize);
            for (int i = 0; i < hinhanh.Count; i++)
            {
                for (int j = 0; j < monan.Count; j++)
                    if (monan[j].MaMonAn == hinhanh[i].MaMonAn)
                        monan[j].LinkHinhAnh.Add(hinhanh[i].LinkHinhAnh);
            }
            
            return View(monan);
           
        }

        //
        // GET: /About/
        public ActionResult About(string UserName)
        {
            ViewBag.UserName = UserName;
            return View();
        }

        //
        // GET: /ListTheoAnGi/
        public ActionResult ListTheoAnGi(string loaimonan, int? page, string UserName)
        {
            ViewBag.UserName = UserName;
            ViewBag.loaimonan = new SelectList(dbhelper.GetLoaiMonAn(), "MaLoaiMonAn", "TenLoai");//.LoaiMonAns.ToList()
            int pageSize = 3;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            List<HinhAnh> hinhanh = dbhelper.GetHinhAnhs(); // ((from ha in db.HinhAnhs
                                      //select ha)).ToList();

            ViewBag.Loai = "tất cả";
            IPagedList<MonAn> monan = //(from ma in db.MonAns
                                       //select ma).OrderByDescending(m => m.TenMonAn)
                                        dbhelper.GetMonAnsDesByName().ToPagedList(pageIndex, pageSize);
            if (!string.IsNullOrWhiteSpace(loaimonan))
            
            {
                int id = Int32.Parse(loaimonan);
                List<LoaiMonAn> lman = dbhelper.GetLoaiMonAnById(id); //(from lma in db.LoaiMonAns
                                 //where lma.MaLoaiMonAn == id
                                 //select lma).ToList();
                ViewBag.Loai = "món " + lman[0].TenLoai;
                monan = //(from ma in db.MonAns
                          //   select ma).Where(ma => ma.LoaiMonAn == id).OrderByDescending(m => m.TenMonAn)
                        dbhelper.GetMonAnsDesByNameAndId(id).ToPagedList(pageIndex, pageSize);
            }

            for (int i = 0; i < hinhanh.Count; i++)
            {
                for (int j = 0; j < monan.Count; j++)
                    if (monan[j].MaMonAn == hinhanh[i].MaMonAn)
                        monan[j].LinkHinhAnh.Add(hinhanh[i].LinkHinhAnh);
            }
            return View(monan);
        }

        //
        // GET: /ListTheoODau/
        public ActionResult ListTheoODau(string odau, int? page, string UserName)
        {
            ViewBag.UserName = UserName;
            int pageSize = 3;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            List<HinhAnh> hinhanh = dbhelper.GetHinhAnhs();//((from ha in db.HinhAnhs
                                      //select ha)).ToList();

            IPagedList<MonAn> listmonan;
            if (string.IsNullOrWhiteSpace(odau))
            {
                ViewBag.Loai = "tất cả";
                listmonan = //(from ma in db.MonAns
                             //select ma).OrderByDescending(m => m.TenMonAn)
                            dbhelper.GetMonAnsDesByName().ToPagedList(pageIndex, pageSize);
            }
            else
            {
                string xuatxu = "";
                if (odau == "bac") xuatxu = "Miền Bắc";
                else if (odau == "trung") xuatxu = "Miền Trung";
                else if (odau == "nam") xuatxu = "Miền Nam";
                else if (odau == "tay") xuatxu = "Miền Tây";
                listmonan = //(from ma in db.MonAns
                            // select ma).Where(ma => ma.XuatXu == xuatxu).OrderByDescending(m => m.TenMonAn)
                            dbhelper.GetMonAnsDesByNameAndXuatxu(xuatxu).ToPagedList(pageIndex, pageSize);
            }

            for (int i = 0; i < hinhanh.Count; i++)
            {
                for (int j = 0; j < listmonan.Count; j++)
                    if (listmonan[j].MaMonAn == hinhanh[i].MaMonAn)
                        listmonan[j].LinkHinhAnh.Add(hinhanh[i].LinkHinhAnh);
            }
            return View(listmonan);
        }

        //
        // GET: /ListTheoSignUp/
        public ActionResult SignUp()
        {
            return View();
        }

        //
        // GET: /ChiTietMonAn/
        public ActionResult ChiTietMonAn(int? id, string UserName)
        {
            ViewBag.UserName = UserName;
            //ViewBag.MaMonAn = id;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int idmonan = (int)id;
            MonAn ma = dbhelper.GetMonAnById(idmonan);//db.MonAns.Find(id);
            List<HinhAnh> hinhanh = dbhelper.GetHinhAnhsById(idmonan);//(from ha in db.HinhAnhs
                                     //where ha.MaMonAn == id
                                     //select ha).ToList();
            List<Comment> dsBinhLuan = dbhelper.GetCommentById(idmonan);//(from bl in db.Comments
                                        //where bl.MaMonAn == id
                                        //select bl).ToList();
            for (int i = dsBinhLuan.Count - 1; i >= 0; i--)
            {
                ma.listBinhLuan.Add(dsBinhLuan[i]);
            }
            for (int i = 0; i < hinhanh.Count; i++)
                ma.LinkHinhAnh.Add(hinhanh[i].LinkHinhAnh);
            if (ma == null)
            {
                return HttpNotFound();
            }
           
            return View(ma);
        }
        [HttpGet]
        public ActionResult GioHang(string UserName)
        {
            ViewBag.UserName = UserName;
            var kq = dbhelper.GetGioHang();//(from gh in db.GioHangTamThois
                     //select gh).ToList();
            var tongtien = kq.Select(m => m.Gia).Sum();
            ViewBag.TongTien = tongtien;
            return View(kq);
        }
        [HttpGet]
        //
        // GET: /TimKiem/
        public ActionResult TimKiem(String pTenMonAn, int? page, string UserName)
        {
            ViewBag.UserName = UserName;
            if (pTenMonAn == null)
                return RedirectToAction("Index", "MonAn");
            int pageSize = 3;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            List<HinhAnh> hinhanh = dbhelper.GetHinhAnhs();//((from ha in db.HinhAnhs
                                     // select ha)).ToList();

            IPagedList<MonAn> monan = //(from ma in db.MonAns
                                       //select ma).Where(ma => ma.TenMonAn.Contains(pTenMonAn)).OrderByDescending(m => m.TenMonAn)
                                    dbhelper.GetMonAnsContainName(pTenMonAn).ToPagedList(pageIndex, pageSize);
            for (int i = 0; i < hinhanh.Count; i++)
            {
                for (int j = 0; j < monan.Count; j++)
                    if (monan[j].MaMonAn == hinhanh[i].MaMonAn)
                        monan[j].LinkHinhAnh.Add(hinhanh[i].LinkHinhAnh);
            }
            return View(monan);
            
        }

        
        [HttpGet]
        public ActionResult TimKiemNangCao(String XuatXu, String LoaiMonAn, String name_search, int? page, string UserName)
        {
            ViewBag.UserName = UserName;
            ViewBag.LoaiMonAn = Request["cbLoaiMonAn"] == null ? LoaiMonAn : Request["cbLoaiMonAn"];
            ViewBag.XuatXu = Request["odau"] == null ? XuatXu : Request["odau"];
            int pageSize = 3;
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            List<HinhAnh> hinhanh = dbhelper.GetHinhAnhs(); //((from ha in db.HinhAnhs
                                     // select ha)).ToList();
            var _ma = dbhelper.GetMonAn();//from ma in db.MonAns
                     //select ma;
            if (name_search != null)
                _ma = _ma.Where(ma => ma.TenMonAn.Contains(name_search));
            if (Request["cbLoaiMonAn"] == "heo" || LoaiMonAn == "heo")
                _ma = _ma.Where(ma => ma.LoaiMonAn == 1);
            if (Request["cbLoaiMonAn"] == "bo" || LoaiMonAn == "bo")
                _ma = _ma.Where(ma => ma.LoaiMonAn == 2);
            if (Request["cbLoaiMonAn"] == "ga" || LoaiMonAn == "ga")
                _ma = _ma.Where(ma => ma.LoaiMonAn == 3);
            if (Request["cbLoaiMonAn"] == "haisan" || LoaiMonAn == "haisan")
                _ma = _ma.Where(ma => ma.LoaiMonAn == 4);
            if (Request["odau"] == "bac" || XuatXu == "bac")
                _ma = _ma.Where(ma => ma.XuatXu == "Miền Bắc");
            if (Request["odau"] == "trung" || XuatXu == "trung")
                _ma = _ma.Where(ma => ma.XuatXu == "Miền Trung");
            if (Request["odau"] == "nam" || XuatXu == "nam")
                _ma = _ma.Where(ma => ma.XuatXu == "Miền Nam");
            if (Request["odau"] == "tay" || XuatXu == "tay")
                _ma = _ma.Where(ma => ma.XuatXu == "Miền Tây");

            IPagedList<MonAn> monan =
                _ma.OrderByDescending(m => m.TenMonAn).ToPagedList(pageIndex, pageSize);
            //IPagedList<MonAn> monan = (from ma in db.MonAns
            //                           select ma).Where(ma => ma.XuatXu == "Miền Nam").OrderByDescending(m => m.TenMonAn).ToPagedList(pageIndex, pageSize);
                
            for (int i = 0; i < hinhanh.Count; i++)
            {
                for (int j = 0; j < monan.Count; j++)
                    if (monan[j].MaMonAn == hinhanh[i].MaMonAn)
                        monan[j].LinkHinhAnh.Add(hinhanh[i].LinkHinhAnh);
            }
            return View(monan);
        }


       
        //
        // GET: /Create/
        [HttpGet]
        public ActionResult Create(string UserName)
        {
            ViewBag.UserName = UserName;

            return View();
        }

        [HttpPost]
        public ActionResult Create(_1112466_1112492.Models.MonAnModel model, string UserName)
        {
            ViewBag.UserName = UserName;
            QLMonAn.MonAn ma = new QLMonAn.MonAn();
            try
            {
                ma.TenMonAn = model.TenMonAn;
                ma.DonGia = Int32.Parse(model.DonGia);
                ma.MoTa = model.MoTa;
                if (Request["cbLoaiMonAn"] == "heo")
                {
                    ma.LoaiMonAn = 1;
                }
                else if (Request["cbLoaiMonAn"] == "bo")
                    ma.LoaiMonAn = 2;
                else if (Request["cbLoaiMonAn"] == "ga")
                    ma.LoaiMonAn = 3;
                else if (Request["cbLoaiMonAn"] == "haisan")
                    ma.LoaiMonAn = 4;

                if (Request["tinhtrang"] == "ss")
                    ma.TinhTrang = true;
                else
                    ma.TinhTrang = false;

                if (Request["odau"] == "bac")
                    ma.XuatXu = "Miền Bắc";
                else if (Request["odau"] == "trung")
                    ma.XuatXu = "Miền Trung";
                else if (Request["odau"] == "nam")
                    ma.XuatXu = "Miền Nam";
                else if (Request["odau"] == "tay")
                    ma.XuatXu = "Miền Tây";
                ma.SoLuongBan = 0;
                dbhelper.AddMonAn(ma);
               // db.MonAns.Add(ma);
               // db.SaveChanges();

                int id = ma.MaMonAn;

                QLMonAn.HinhAnh ha = new QLMonAn.HinhAnh();
                ha.MaMonAn = id;
                ha.LinkHinhAnh = model.linkhinhanh1;
                dbhelper.AddHinhAnh(ha);
                //db.HinhAnhs.Add(ha);
                //db.SaveChanges();

                if (model.linkhinhanh2 != null)
                {
                    ha.MaMonAn = id;
                    ha.LinkHinhAnh = model.linkhinhanh2;
                    dbhelper.AddHinhAnh(ha);
                    //db.HinhAnhs.Add(ha);
                    //db.SaveChanges();
                }

                if (model.linkhinhanh3 != null)
                {
                    ha.MaMonAn = id;
                    ha.LinkHinhAnh = model.linkhinhanh3;
                    dbhelper.AddHinhAnh(ha);
                    //db.HinhAnhs.Add(ha);
                    //db.SaveChanges();
                }
            }
            catch (DbEntityValidationException ex)
            {

            }



            return RedirectToAction("Index", "MonAn");
        }

        [HttpGet]
        public ActionResult Edit(int? id, string UserName)
        {
            ViewBag.UserName = UserName;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            int idmonan = (int)id;
            MonAn ma = dbhelper.GetMonAnById(idmonan);//db.MonAns.Find(id);
            List<HinhAnh> hinhanh = dbhelper.GetHinhAnhsById(idmonan);//(from ha in db.HinhAnhs
                                     //where ha.MaMonAn == id
                                     //select ha).ToList();
            
            for (int i = 0; i < hinhanh.Count; i++)
                ma.LinkHinhAnh.Add(hinhanh[i].LinkHinhAnh);
            if (ma == null)
            {
                return HttpNotFound();
            }
            _1112466_1112492.Models.MonAnModel model = new Models.MonAnModel();
            model.MaMonAn = ma.MaMonAn;
            model.ha1 = hinhanh[0];
            if(hinhanh.Count >=2)
                model.ha2 = hinhanh[1];
            if (hinhanh.Count >= 3)
                model.ha3 = hinhanh[2];
            model.MaMonAn = ma.MaMonAn;
            model.TenMonAn = ma.TenMonAn;
            model.DonGia = ma.DonGia.ToString();
            model.MoTa = ma.MoTa;
            model.TinhTrang = (bool)ma.TinhTrang;
            model.LoaiThucAn = (int)ma.LoaiMonAn;
            model.XuatXu = ma.XuatXu;
            model.linkhinhanh1 = ma.LinkHinhAnh[0];
            if(ma.LinkHinhAnh.Count >= 2)
                model.linkhinhanh2 = ma.LinkHinhAnh[1];
            if (ma.LinkHinhAnh.Count >= 3)
                model.linkhinhanh3 = ma.LinkHinhAnh[2];
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(_1112466_1112492.Models.MonAnModel model, int? idMA, string UserName)
        {
            ViewBag.UserName = UserName;
            int idmonan = (int)idMA;
            var orginal = dbhelper.GetMonAnById(idmonan);// db.MonAns.Find(idMA);


            orginal.TenMonAn = model.TenMonAn;
            orginal.DonGia = Int32.Parse(model.DonGia);
            orginal.MoTa = model.MoTa;
            if (Request["cbLoaiMonAn"] == "heo")
            {
                orginal.LoaiMonAn = 1;
            }
            else if (Request["cbLoaiMonAn"] == "bo")
                orginal.LoaiMonAn = 2;
            else if (Request["cbLoaiMonAn"] == "ga")
                orginal.LoaiMonAn = 3;
            else if (Request["cbLoaiMonAn"] == "haisan")
                orginal.LoaiMonAn = 4;

            if (Request["tinhtrang"] == "ss")
                orginal.TinhTrang = true;
            else
                orginal.TinhTrang = false;

            if (Request["odau"] == "bac")
                orginal.XuatXu = "Miền Bắc";
            else if (Request["odau"] == "trung")
                orginal.XuatXu = "Miền Trung";
            else if (Request["odau"] == "nam")
                orginal.XuatXu = "Miền Nam";
            else if (Request["odau"] == "tay")
                orginal.XuatXu = "Miền Tây";
            db.SaveChanges();
            
            int id = (int)idMA;


            
            
            return RedirectToAction("ChiTietMonAn", "MonAn", new { id = idMA});
        }
        [HttpPost]
        public ActionResult ChiTietMonAn(_1112466_1112492.Models.comment comment)
        {
            int IDMonAn = comment.IDMonAn;
            int STT = comment.STT;
            string Ten = comment.Ten;
            string Binh_Luan = comment.Binh_Luan;
            string Ngay = comment.Ngay;

            QLMonAn.Comment cm = new QLMonAn.Comment();

            List<Comment> c = dbhelper.GetCommentById(IDMonAn);//(from com in db.Comments
                               //where com.MaMonAn == IDMonAn
                               //select com).ToList();
            STT = 1;
            if (c == null)
            {
                STT = 1;
            }
            else
            {
                for (int i = 0; i < c.Count; i++)
                {
                    if (c[i].STT >= STT)
                    {
                        STT += 1;
                    }
                }
            }

            cm.MaMonAn = IDMonAn;
            cm.STT = STT;
            cm.Ten = Ten;
            cm.Binh_Luan = Binh_Luan;
            cm.Ngay = Ngay;

            //db.Comments.Add(cm);
            //db.SaveChanges();
            dbhelper.AddComment(cm);

            return Json(new { success = true });
        }
        [HttpGet]
        public ActionResult LoadBinhLuan()
        {
            var data = dbhelper.GetCommentById(1);//(from co in db.Comments
                        //where co.MaMonAn == 1
                        //select co).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Delete(int? id, string UserName)
        {
            ViewBag.UserName = UserName;
            int idmonan = (int)id;
            QLMonAn.MonAn ma = dbhelper.GetMonAnById(idmonan);//db.MonAns.Find(id);
            dbhelper.RemoveMonAn(ma);
            //db.MonAns.Remove(ma);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult DeleteAd(int? id, string UserName)
        {
            int idmonan = (int)id;
            QLMonAn.MonAn ma = dbhelper.GetMonAnById(idmonan);//db.MonAns.Find(id);
            dbhelper.RemoveMonAn(ma);
            //db.MonAns.Remove(ma);
            //db.SaveChanges();

            return RedirectToAction("Admin", new { UserName = UserName });
        }
        [HttpGet]
        public ActionResult DeleteDDH(int? id)
        {
            int idddh = (int)id;
            QLMonAn.DonDatHang ddh = dbhelper.GetDonDatHangById(idddh);//db.DonDatHangs.Find(id);
            dbhelper.RemoveDDH(ddh);
            //db.DonDatHangs.Remove(ddh);
            //db.SaveChanges();

            return RedirectToAction("Admin");
        }
        public ActionResult add(int? id, string UserName)
        {
            ViewBag.UserName = UserName;
            int idmonan = (int)id;
            MonAn ma = dbhelper.GetMonAnById(idmonan);//db.MonAns.Find(id);
            GioHangTamThoi temp = new GioHangTamThoi();
            temp.MaMonAn = id;
            temp.TenMonAn = ma.TenMonAn;
            temp.Gia = ma.DonGia;
            dbhelper.AddGioHang(temp);
            //db.GioHangTamThois.Add(temp);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult DeleteCart(int? id)
        {
            int idgh = (int)id;
            GioHangTamThoi temp = dbhelper.GetGioHangById(idgh);//db.GioHangTamThois.Find(id);
            dbhelper.RemoveGioHang(temp);
            //db.GioHangTamThois.Remove(temp);
            //db.SaveChanges();
            return RedirectToAction("GioHang");
        }
       
        public ActionResult ThanhToan(string UserName)
        {
            _1112466_1112492.Models.ThanhToanModel model = new Models.ThanhToanModel();
            ViewBag.UserName = UserName;
            var kq = dbhelper.GetGioHang();//(from gh in db.GioHangTamThois
                      //select gh).ToList();
            var tongtien = kq.Select(m => m.Gia).Sum();
            model.SoTien = (int)tongtien;
            AspNetUser user = dbhelper.GetUserByUserName(UserName);//(from u in db.AspNetUsers
                               //where u.UserName == UserName
                               //select u).FirstOrDefault();
            model.TenTaiKhoan = UserName;
            model.Email = user.Email;
            model.SoDienThoai = user.DienThoai;
           
            
            return View(model);
        }

        [HttpPost]
        public ActionResult ThanhToan(string UserName, _1112466_1112492.Models.ThanhToanModel model)
        {
            AspNetUser user = dbhelper.GetUserByUserName(model.TenTaiKhoan);//(from u in db.AspNetUsers
                               //where u.UserName == model.TenTaiKhoan
                               //select u).FirstOrDefault();
            DonDatHang ddh = new DonDatHang();
            ddh.NgayGiaoHang = Convert.ToDateTime(model.NgayGiaoHang);
            ddh.DonGia = model.SoTien;
            ddh.TinhTrang = false;
            ddh.TaiKhoanDat = model.TenTaiKhoan;
            dbhelper.AddDDH(ddh);
            //db.DonDatHangs.Add(ddh);
            //db.SaveChanges();
            List<GioHangTamThoi> gh = new List<GioHangTamThoi>();
            gh = dbhelper.GetGioHang();//(from gh1 in db.GioHangTamThois
                  //select gh1).ToList();
            List<QLMonAn.MonAn> monans = new List<MonAn>();
            for (int i = 0; i < gh.Count; i++)
            {
                MonAn ma = dbhelper.GetMonAnById(gh[i].Id);//db.MonAns.Find(gh[i].Id);
                ma.SoLuongBan += 1;
                db.SaveChanges();
                //monans.Add(ma);
            }
            
            ViewBag.UserName = UserName;
            return RedirectToAction("Index", new {UserName = UserName});
        }

        public ActionResult Admin(string UserName)
        {
            ViewBag.UserName = UserName;
            _1112466_1112492.Models.AdminModel model = new Models.AdminModel();
            model.users = dbhelper.GetUser();//(from user in db.AspNetUsers
                           //select user).ToList();
            model.monans = dbhelper.Getmonan();//(from ma in db.MonAns
                           // select ma).ToList();
            model.ddhs = dbhelper.GetDDH();//(from ddh in db.DonDatHangs
                          //select ddh).ToList();
            return View(model);
        }
        
        public ActionResult TaiKhoan(string Name)
        {
            _1112466_1112492.Models.RegisterViewModel model = new Models.RegisterViewModel();
            AspNetUser user = dbhelper.GetUserByUserName(Name);//(from u in db.AspNetUsers
                               //where u.UserName == Name
                               //select u).FirstOrDefault();
            model.NgaySinh = user.NgaySinh;
            model.DienThoai = user.DienThoai;
            model.Email = user.Email;
            return View(model);
        }

        [HttpPost]
        public ActionResult SuaTaiKhoan(_1112466_1112492.Models.RegisterViewModel model, string UserName)
        {
            AspNetUser user = dbhelper.GetUserByUserName(UserName);//(from u in db.AspNetUsers
                             // where u.UserName == UserName
                             // select u).FirstOrDefault();
            user.NgaySinh = model.NgaySinh;
            user.DienThoai = model.DienThoai;
            db.SaveChanges();
            return RedirectToAction("TaiKhoan", new { Name = UserName});
        }

        [HttpGet]
        public ActionResult ThongKe()
        {
            _1112466_1112492.Models.ThongKeModel model = new Models.ThongKeModel();
            model.monans = dbhelper.GetMonAnTop10();//(from ma in db.MonAns
                            //select ma).OrderByDescending(m => m.SoLuongBan).ToList().Take(10);
            model.ddhs = dbhelper.GetDDH();//(from ddh in db.DonDatHangs
                          //select ddh).ToList();
            for (int i = 0; i < model.ddhs.Count; i++)
            {
                model.DoanhSo += (int)model.ddhs[i].DonGia;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult ThongKe(String txtthongke)
        {
            _1112466_1112492.Models.ThongKeModel model = new Models.ThongKeModel();
            model.monans = dbhelper.GetMonAnTop10();//(from ma in db.MonAns
                           // select ma).OrderByDescending(m => m.SoLuongBan).ToList().Take(10);
            
            return View(model);
        }
	}
}