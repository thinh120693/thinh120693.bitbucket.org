﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(_1112466_1112492.Startup))]
namespace _1112466_1112492
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
