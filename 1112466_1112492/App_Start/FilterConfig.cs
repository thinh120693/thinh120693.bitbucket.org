﻿using System.Web;
using System.Web.Mvc;

namespace _1112466_1112492
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
